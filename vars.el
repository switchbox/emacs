;; Misc variables that aren't set in customize (theme.el).

;;;; system puts
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'erase-buffer 'disabled nil)

;;;; setq's
(setq initial-buffer-choice t)
(setq my-ruby-version "2.2.5")
(setq initial-scratch-message ";; SCRATCH LISP BUFFER\n\n")
(setq linum-format " %d  ")
(setq make-backup-files nil)
(setq-default indent-tabs-mode nil)
(setq emacs-path "~/.emacs.d/")
(setq notes-path "~/proj/notes/")
(setq my-current-project-path "~/proj/")
(setq org-src-fontify-natively t)
(setq default-cursor-in-non-selected-windows nil)
(setq blink-cursor-blinks 0)
(setq solarized-high-contrast-mode-line t)
(load-theme 'solarized-dark)
(setq default-cursor-type 'box)
(setq my-cur-long-lines nil)
(setq my-cur-long-lines-set nil)
(setq debug-on-error t)

;;;; global funs
(global-auto-complete-mode)
(global-visual-line-mode t)
(global-linum-mode 0)
(global-rinari-mode)
(global-flycheck-mode)

;;;; emacs env funs (theme etc)
(set-fringe-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(windmove-default-keybindings)
(blink-cursor-mode 0)
(set-cursor-color "red")
(set-face-attribute 'default nil :height 140 :family "Terminus")
(column-number-mode t)

;;;; misc funs
(my-rvm-default)
(projectile-mode)
