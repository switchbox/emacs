;; Custom hooks.

;;;; Linum hooks. Only enter linum mode on files and not other buffers.
;; (add-hook 'find-file-hook 'linum-mode)
;; (add-hook 'dired-find-file-hook 'linum-mode)
;; (add-hook 'fiplr-find-file 'linum-mode)
