;; all custom defuns belong here. I prefer making defuns
;; even for one line commands over bindings/lambdas simply for
;; separation of concerns. It's easier to generate tags this way
;; in order to jump straight from key binding to its defun.

(defun my-mark-line ()
  "Mark the current line."
  (interactive)
  (back-to-indentation)
  (set-mark (point))
  (end-of-line))

(defun my-close-paren ()
  "Go to next ) parenthesis and enter another"
  (interactive)
  (save-excursion
    (search-forward ")")
    (insert ")")))

(defun my-close-paren-semi ()
  "Go to next ; and enter )"
  (interactive)
  (save-excursion
    (search-forward ";")
    (backward-char)
    (insert ")")))

(defun my-insert-semi ()
  "Insert semi at end of line & stay in place"
  (interactive)
  (save-excursion
    (end-of-line)
    (insert ";")))

(defun my-insert-comma ()
  "Insert comma at end of line & stay in place"
  (interactive)
  (save-excursion
    (end-of-line)
    (insert ",")))

(defun my-comment-line ()
  "Comments/uncomments the current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

;; make only for org mode
(defun my-org-source-code (type)
  "Insert source code markers for org mode"
  (interactive "sEnter language: ")
  (insert "#+BEGIN_SRC " type)
  (newline)
  (insert "#+END_SRC")
  (previous-line)
  (end-of-line)
  (newline))

(defun my-indent-all ()
  "Indents, trims whitespace of, and formats line returns on entire buffer.
Not intended for use with other people's files, rather when creating a new
file or when verison control doesn't need to be taken into consideration."
  (interactive)
  (let (start end)
    (if mark-active
        (setq start (region-beginning) end (region-end))
      (setq start (point-min) end (point-max)))
    (indent-region start end)
    (whitespace-cleanup-region start end)
    (my-cleanup-line-returns start end)
    (message "[%s] indented & trimmed." buffer-file-name)))

(defun my-resize-right ()
  "Shift window right x 20"
  (interactive)
  (enlarge-window-horizontally 20))

(defun my-resize-left ()
  "Shift window left x 20 (-20)"
  (interactive)
  (enlarge-window-horizontally -20))

(defun my-resize-up ()
  "Resize up"
  (interactive)
  (enlarge-window 15))

(defun my-resize-down ()
  "Resize down"
  (interactive)
  (enlarge-window -15))

(defun my-source-init ()
  "Reload emacs configuration by loading init file"
  (interactive)
  (load (my-emacs-file "init")))

(defun my-next-defun ()
  "Go to top of next method/fun"
  (interactive)
  (end-of-defun)
  (end-of-defun)
  (beginning-of-defun))

(defun my-kill-char (c)
  "Search forward and delete char - save cursor pos"
  (interactive "sEnter char to kill: ")
  (save-excursion
    (search-forward c)
    (delete-char -1)))

(defun my-open-scratch ()
  "Open scratch buffer"
  (interactive)
  (switch-to-buffer "*scratch*"))

(defun my-open-notes (project)
  "Lazily open project notes by name & create directory"
  (interactive "sEnter project name: ")
  (unless (file-exists-p notes-path)
    (make-directory notes-path))
  (find-file (concat notes-path project ".org")))

(defun config (file)
  "Open config file/directory. When blank argument is passed as
file dired opens the emacs-path/, however when a file is
specified it opens that config file. After opening file/dired
tags table is generated."
  (interactive
   (list (read-file-name
          "Which config file would you like to open? "
          emacs-path)))
  (if (string= file "")
      (dired emacs-path)
    (progn
      (let ((f file))
        (if (file-exists-p f)
            (find-file f)
          (message "File does not exist."))))))

(defun defuns ()
  "open defuns file in .emacs.d"
  (interactive)
  (find-file (my-emacs-file "defuns")))

(defun keys ()
  "open global key map in .emacs.d"
  (interactive)
  (find-file (my-emacs-file "global-keys")))

(defun my-emacs-file (file)
  "Helper method to loading emacs configs"
  (concat emacs-path file ".el"))

(defun my-toggle-last-buffer ()
  "Switch to other buffer & revert if buffer list"
  (interactive)
  (switch-to-buffer (other-buffer))
  (if (string= "*Buffer List*" (buffer-name))
      (my-revert-and-message)))

(defun my-buffer-menu-and-reload ()
  "When viewing buffer menu automatically refresh"
  (interactive)
  (buffer-menu)
  (my-revert-and-message))

(defun my-revert-and-message ()
  "Reload buffer and message. Meant for Buffer List open."
  (revert-buffer)
  (message "Buffer list refreshed."))

(defun my-kill-line ()
  "Literally kill the entire current line"
  (interactive)
  (beginning-of-line)
  (kill-visual-line 1)
  (back-to-indentation))

(defun my-line-below ()
  "Create line below, indent, & move point"
  (interactive)
  (end-of-line)
  (newline-and-indent))

(defun my-line-above ()
  "Create line above, indent, & move point"
  (interactive)
  (if (= (point) (point-min))
      ;; when we are at point min we can't go
      ;; to the previous line off the bat we have
      ;; to create the new line first.
      (progn
        (newline)
        (previous-line))
    (progn
      (previous-line)
      (my-line-below))))

(defun my-dired-current ()
  "View dired for cwd"
  (interactive)
  (dired "."))

(defun my-split-right ()
  "When splitting, show other buffer"
  (interactive)
  (split-window-right)
  (other-window 1)
  (switch-to-buffer (other-buffer)))

(defun my-split-below ()
  "When splitting, show other buffer"
  (interactive)
  (split-window-below)
  (other-window 1)
  (switch-to-buffer (other-buffer)))

(defun my-cut-from-indentation ()
  "Move point to first non whitespace char & cut line"
  (interactive)
  (back-to-indentation)
  (kill-visual-line))

(defun my-open-proj (dir)
  "Open a project directory"
  (interactive
   (list (read-directory-name
          "Select a project directory: "
          "~/proj/")))
  (if (file-exists-p dir)
      (dired dir)
    (message "Path does not exist!")))

(defun my-rails-tags ()
  "Create rails tags table"
  (interactive)
  (compile "ctags -eR app/controllers/ app/helpers/ app/models/")
  (message "Tags generated."))

(defun my-kill-word ()
  "Evaluate the char before and delete the following word accordingly.
If the previous char is a-z, go to backward word and delete word
from that point. If not, just delete the word from tha that point.
E.g. -<point>apple wouldn't move the point however app<point>le would
move to the beginning as <point>apple and delete the word."
  (interactive)
  (let ((c (string(char-before))))
    (when (string-match "[a-zA-Z]" c)
      (backward-word))
    (kill-word 1)))

(defun my-open-current-project ()
  "Open current project directory set in vars."
  (interactive)
  (dired my-current-project-path)
  (my-display-git-branch))

(defun my-open-project-directory ()
  "Simply open ~/proj"
  (interactive)
  (dired "~/proj"))

(defun my-region-under-to-camel (beg end)
  "Convert region from underscore case to camel case."
  (interactive "r")
  (save-excursion
    (goto-char beg)
    (while (< (point) end)
      (if (string= "_" (string (following-char)))
          (progn
            (delete-char 1)
            (capitalize-word 1))
        (forward-char)))))

(defun my-region-camel-to-under (beg end)
  "Convert region from camel case to underscore case"
  (interactive "r")
  (setq case-fold-search nil)
  (save-excursion
    (goto-char beg)
    (while (< (point) end)
      (when (string-match "[A-Z]" (string (following-char)))
        (insert "_"))
      (forward-char)))
  (downcase-region beg end)
  (setq case-fold-search t))

;; this is rediculous and should be changed into one function.
;; define method helper & see if 'mark-active' is t/f
(defun my-replace-char (char)
  "Replace char under point with specified char"
  (interactive "cReplace char with: ")
  (let ((c (string char)))
    (if (string= "" c)
        (message "Nothing changed")
      (progn
        (save-excursion
          (delete-char 1)
          (insert c))))))

(defun my-replace-region (c beg end)
  "Replace each char in region with specified"
  (interactive "sReplace region with: \nr")
  (if (string= "" c)
      (message "Nothing changed")
    (progn
      (save-excursion
        (goto-char beg)
        (while (< (point) end)
          (delete-char 1)
          (insert c))))))

(defun my-surround (c)
  "Surround a region or word under point with provided character. When an open/close character
Such as a paren. or bracket is provided, eiter opening or closing bracket may be provided
and the correct open/close will be insert in the right place."
  (interactive "cSurround with: ")
  (let* ((char (char-to-string c))
         (sets (my-set-list char))
         (open (nth 0 sets))
         (close (nth 1 sets))
         (beg (and mark-active (region-beginning)))
         (end (and mark-active (region-end))))
    (save-excursion
      (if beg
          (goto-char beg)
        (when (my-move-back)
          (backward-word)))
      (insert open)
      (if end
          (progn
            (goto-char end)
            (forward-char))
        (forward-word))
      (insert close))))

(defun my-move-back ()
  "Determine whether current word is beginning of 'word'"
  (interactive)
  (let ((c (string (preceding-char))))
    (string-match "[a-z0-9]" c)))

(defun my-delete-indent ()
  (interactive)
  (save-excursion
    (back-to-indentation)
    (while (> (point) (line-beginning-position))
      (delete-char -1))))

(defun my-forward-word ()
  "Move to beg of next word unless mark is active, then use default functionality.
When the following char is not a-Z0-9 it will simply move to the next letter/number
instead of jumping all together. Otherwise if you're at the beginning of the line,
forward-word would jump to the 2nd word on the line, not literally the next word."
  (interactive)
  (let ((cur (string (following-char))))
    (if mark-active
        (forward-word)
      (progn
        (if (string-match "[a-zA-Z0-9]" cur)
            (progn
              (forward-word 2)
              (backward-word))
          (while (not (string-match "[a-zA-Z0-9]" (string (following-char))))
            (forward-char)))))))

(defun my-open-terminal ()
  "Open term with bash"
  (interactive)
  (term "/bin/bash"))

(defun my-open-eshell ()
  "Open eshell"
  (interactive)
  (eshell))

(defun my-get-hour ()
  "Simply gets the current hour from parsed list & returns integer of current hour."
  (let* ((time (nth 3 (split-string (current-time-string))))
         (hour (nth 0 (split-string time ":"))))
    (string-to-number hour)))

(defun my-set-theme ()
  "Change from solarized light to dark after time set in vars."
  (setq my-theme-change-time "11:00")
  (if (>= (my-get-hour) (string-to-number my-theme-change-time))
      (load-theme 'solarized-dark)
    (load-theme 'solarized-light))
  (set-cursor-color "red"))

(defun my-cleanup-line-returns (start end)
  "Scans file and formats line returns to at max 2 returns. This only allows lines
to have a single space before going on to the next line."
  (interactive)
  (let ((flag t))
    (save-excursion
      (while flag
        (goto-char start)
        (let ((line (re-search-forward "^\n\n+" end t)))
          (if line
              (kill-visual-line -1)
            (setq flag nil)))))))

(defun my-duplicate-line ()
  "Simply duplicates current line & yanks below"
  (interactive)
  (let ((col (current-column)))
    (kill-ring-save (line-beginning-position) (line-end-position))
    (end-of-line)
    (newline)
    (yank)
    (move-to-column col)))

(defun my-describe-keys ()
  "Displays documentation for my-keys list for all custom keys."
  (interactive)
  (describe-variable 'my-keys))

(defun my-set-list (char)
  "Helper function to evaluate a char argument for matching char sets.
In other words, if '[' is passed to my-set-list, it will return '( [ ] ).
If it is not a character in the set list it will return back whatever was passed
to it as an argument (char). This is useful for functions that need to accept
either an open or closing character when operating on text."
  (let ((set '(( "{" "}" )
               ( "(" ")" )
               ( "[" "]" )
               ( "<" ">" )))
        (open)
        (close))
    (dolist (s set)
      (when (or (string= char (nth 0 s))
                (string= char (nth 1 s)))
        (setq open (nth 0 s) close (nth 1 s))))
    (unless (and open close)
      (setq open char close char))
    (list open close)))

(defun my-append-to-defuns (beg end)
  "Copy a region typically from scratch buffer and append to defuns.el.
This is a quick way to get a completed function copied over, saved, and
evaluated. If defuns.el is not currently open it this function will not
work. This is atypical since when writing new defuns the defuns.el buffer
is usually already open."
  (interactive "r")
  (let ((text (buffer-substring beg end))
        (newbuf (get-buffer "defuns.el")))
    (save-excursion
      (set-buffer newbuf)
      (goto-char (point-max))
      (insert text)
      (save-buffer)
      (eval-buffer))))

(defun my-kill-in-tags ()
  "Kill content in HTML type tags"
  (interactive)
  (let ((start)
        (end))
    (save-excursion
      (when (search-backward ">" nil t)
        (setq start (+ (point) 1)))
      (when (search-forward "<" nil t)
        (setq end (- (point) 1))))
    (if (and start end)
        (progn
          (kill-region start end)
          (when (not (string= "<" (string (following-char))))
            (search-backward "<" nil t)))
      (message "Invalid."))))

(defun my-eval-buffer ()
  "After evaluating buffer display message instead of nothing at all."
  (interactive)
  (eval-buffer)
  (message "Buffer evaluated."))

(defun my-kill-in-parens-line ()
  "Kill line's paren contents. Meant mainly for arguments. This will lazily search both forward & backward
so it doesn't matter what the point position is when calling the function."
  (interactive)
  (let (pnt)
    (save-excursion
      (if (search-forward "(" (line-end-position) t)
          (setq pnt (point))
        (when (search-backward ")" (line-beginning-position) t)
          (setq pnt (point)))))
    (if pnt
        (progn
          (goto-char pnt)
          (my-kill-in (string-to-char "(")))
      (message "No parens found on current line!"))))

(defun my-change-surrounding (cfrom cto)
  "Change set of chars to another. E.g. change surrounding single quotes to double quickly."
  (interactive "cFrom: \ncTo: ")
  (let ((fr (char-to-string cfrom))
        (to (char-to-string cto)))
    (save-excursion
      (unless (string= fr (string (following-char)))
        (search-backward fr (line-beginning-position) t))
      (my-replace-char cto)
      (when (search-forward fr (line-end-position) t)
        (backward-char)
        (my-replace-char cto)))))

(defun my-change-surrounding-to-single ()
  "Wrapper for calling my-change-surrounding converting to single quote"
  (interactive)
  (let ((from (string-to-char "\""))
        (to (string-to-char "'")))
    (my-change-surrounding from to)))

(defun my-change-surrounding-to-double ()
  "Wrapper for calling my-change-surrounding converting to double quote"
  (interactive)
  (let ((from (string-to-char "'"))
        (to (string-to-char "\"")))
    (my-change-surrounding from to)))

(defun my-rvm-default ()
  (interactive)
  (rvm-use my-ruby-version "")
  (message "Rvm: %s" my-ruby-version))

(defun my-config-github-root ()
  "View root dir for emacs.d GitHub repo"
  (interactive)
  (browse-url "https://www.github.com/tysteiman/.emacs.d/"))

(defun my-config-github-file ()
  "View current file on Github"
  (interactive)
  (let* (found
         (files init-files-list)
         (url "https://www.github.com/tysteiman/.emacs.d/")
         (path (buffer-file-name))
         (file (car (last (split-string path "/"))))
         (root (car (split-string file "\\.")))
         (ext (car (last (split-string file "\\.")))))
    (when (string= "el" ext)
      (dolist (f files)
        (when (or
               (string= (format "%s" f) root)
               (string= "init" (format "%s" root)))
          (setq found t)))
      (when found
        (setq url (concat url "blob/master/" file))))
    (browse-url url)))

(defun my-github ()
  "Handler for deciding whether to view root or individual file on GitHub emacs.d repo"
  (interactive)
  (if (buffer-file-name)
      (my-config-github-file)
    (my-config-github-root)))

(defun my-closing-char (c)
  "Find the closing sequence for current nested level. This will return two
points in the buffer: the the starting point of the nested sequence and the
point of the end of the nested sequence. This should be completely traversing
nested structures and returning the parent's closing sequence. Right now error
handling is not good enough and some results are other than expected. When
debugging functions that use this and are operating on the wrong region,
it's likely the problem is here and not in the wrapper function."
  (let* (start
         (itr 0)
         (char (or (char-to-string c)
                   "("))
         (set (my-set-list char))
         (open (car set))
         (close (car (cdr set))))
    (save-excursion
      (when (search-backward open nil t)
        (forward-char)
        (setq start (point))
        (while (not (eq itr 1))
          (if (and
               (string= open (string (following-char)))
               (string= close (string (following-char))))
              (setq itr 1)
            (progn
              (when (string= open (string (following-char)))
                (setq itr (- itr 1)))
              (when (string= close (string (following-char)))
                (setq itr (+ itr 1)))))
          (forward-char))
        (list start (- (point) 1))))))

(defun my-kill-in (c)
  "Kill everything within region returned from my-closing-char. This is
primarily only a wrapper around that function due to the fact that
my-closing-char is only responsible for returning back two locations in the
buffer. This is a part of an effort to break apart my functions and leverage
them individually instead of defining a lot of similar functionality over
and over again."
  (interactive "cKill in: ")
  (let* ((pair (my-closing-char c))
         (start (car pair))
         (end (car (cdr pair))))
    (kill-region start end)))

(defun my-display-git-branch ()
  "Simply displays current git branch"
  (interactive)
  (let ((branch (replace-regexp-in-string "\n" "" (shell-command-to-string "git branch | grep '* ' | sed s-'* '--"))))
    (message "Git: %s" branch)))

(defun my-php-server ()
  "Start a php server on port 8000"
  (interactive)
  (compile "php -S localhost:8000")
  (save-excursion
    (set-buffer "*compilation*")
    (rename-buffer "*php server*")))

(defun my-long-lines (&optional max-length)
  "Mark lines longer than amount max-length chars or 80 by default. This will also delete
any current overlays in the buffer before execution so the overlays are refreshed."
  (interactive)
  (let (cur-reg
        len
        (marks '())
        (max (or max-length
                 81)))
    (save-excursion
      (remove-overlays)
      (goto-char (point-min))
      (while (< (point) (point-max))
        (setq cur-reg
              (buffer-substring (line-beginning-position) (line-end-position)))
        (setq len (length cur-reg))
        (when (> len max)
          (progn
            (forward-char max)
            (if marks
                (setq marks (cons (list (point) (line-end-position)) marks))
              (setq marks (list (list (point) (line-end-position)))))))
        (beginning-of-line)
        (next-line)))
    (if marks
        (progn
          (my-overlay-on-regions (reverse marks))
          (setq my-cur-long-lines nil)
          (setq my-cur-long-lines-set (reverse marks))
          (message "Overlays have been applied."))
      (message "Good job, no long lines were found."))))

(defun my-overlay-on-regions (li)
  "Apply overlay on list of regions. This is not resposnible for any other tasks like removing
existing overlays or determining where the overlays should be placed. It literally is just an engine
for painting the overlays."
  (dolist (reg li)
    (let* ((start (car reg))
           (end (car (cdr reg)))
           (x (make-overlay start end)))
      ;; set the background color to a variable that can be set
      (overlay-put x 'face '(:background "darkred")))))

(defun my-remove-overlays ()
  "Simple wrapper for remove-overlays"
  (interactive)
  (setq my-cur-long-lines nil)
  (setq my-cur-long-lines-set nil)
  (remove-overlays))

(defun my-toggle-long-lines ()
  "Toggle through list of overlays applied by my-long-lines. This will only
work if the overlays are currently active. This will cycle through line numbers
in order as it appears in the buffer. This does not alert when at the last
long line of the buffer however that would be good functionality in the future."
  (interactive)
  (let* ((tmp my-cur-long-lines-set)
         (indexes (- (length tmp) 1)))
    (if my-cur-long-lines-set
        (progn
          (if my-cur-long-lines
              (if (= indexes my-cur-long-lines)
                  (setq my-cur-long-lines 0)
                (setq my-cur-long-lines (+ 1 my-cur-long-lines)))
            (setq my-cur-long-lines 0))
          (goto-char (car (nth my-cur-long-lines tmp))))
      (message "Overlays are not applied..."))))

(defun my-zap-to-char (c)
  "My edit to zap to char. This is exclusive and not inclusive of the char specified."
  (interactive "cZap to: ")
  (let ((cur (point))
        (char (string c)))
    (save-excursion
      (if (search-forward char nil t)
          (kill-region cur (- (point) 1))
        (message "Char %s not found." char)))))

(defun my-quotes-with-comma (type)
  "Insert a set of quotes with comma in buffer"
  (interactive "cType: ")
  (let ((char (string type)))
    (insert char char)
    (backward-char)
    (save-excursion
      (forward-char 1)
      (insert ", " char char))))

(defun my-quotes-with-comma-double ()
  "Quotes with comma - double"
  (interactive)
  (my-quotes-with-comma (string-to-char "\"")))

(defun my-quotes-with-comma-single ()
  "Quotes with common - single"
  (interactive)
  (my-quotes-with-comma (string-to-char "'")))

(defun my-next-quote-set ()
  "Navigate inside next set of quotes"
  (interactive)
  (let ((char (string (following-char))))
    (unless (search-forward char nil t 2)
      (message "Not in buffer!"))))

(defun my-gcc-compile-current-file ()
  "Compile with gcc the current file. This is based on the file name. For example
if the file is main.c it will execute `gcc -o main main.c`. This could be hooked
in later to automatically run that binary however right now that functionality
probably isn't needed."
  (interactive)
  (save-buffer)
  (let (cmd file bin (path (buffer-file-name)))
    (if path
        (setq file (car (last (split-string path "/"))))
      (message "Invalid!"))
    (when file
      (setq bin (car (split-string file "\\.")))
      (setq cmd (concat "gcc -o " bin " " file))
      (compile cmd))))

(defun my-make ()
  "Run make -k on the Makefile in the current directory. This covers the second case
most commonly run in gcc with the file name for testing and make -k."
  (interactive)
  (compile "make -k"))

;; this needs to change because right now you can't have duplicate names in /usr/include/ as in
;; the local directory, /usr/local will always take precedence but shouldn't be the case i'm assuming.
;; we need to actually run code in the when block for if one is found rather than the other.
(defun my-c-open-include-file ()
  "Open file in /usr/include or current directory. This will search for any file and will first check
/usr/include/ before checking the current directory (for quoted include files)"
  (interactive)
  (let (start end
              (srch
               (lambda (ch fwd &optional count)
                 "Search for string fwd/back depending on args. Count will search count times."
                 (if fwd
                     (search-forward ch (line-end-position) t count)
                   (search-backward ch (line-beginning-position) t count)))))
    (save-excursion
      (when (or (funcall srch "<" nil)
                (funcall srch "\"" nil))
        (setq start (+ 1 (point)) end (- (point) 1)))
      (when (or (funcall srch ">" t)
                (funcall srch "\"" t 2))
        (setq end (- (point) 1))))
    (when (and start end)
      (let* ((path "/usr/include/")
             (file (buffer-substring start end))
             (inc (concat path file))
             (local file))
        (if (file-exists-p inc)
            (find-file inc)
          (if (file-exists-p local)
              (find-file local)
            (message "File %s not found!" file)))))))

(defun my-pad-inside (character)
  "Add one space padding between specified char"
  (interactive "cPad between: ")
  (let* ((char (char-to-string character))
         (set (my-set-list char))
         (open (car set))
         (close (car (cdr set))))
    (save-excursion
      (when (search-backward open nil t)
        (forward-char)
        (insert " "))
      (when (search-forward close nil t)
        (backward-char)
        (insert " ")))))

(defun my-align-to-char (beg end character)
  "Sets whitespace before each line in region based on of the specified char of
the line with highest col number for char. This is acheived by first
cycling through entire region, and evaluating whether current line's
column is greater than placeholder. If it is that becomes the new max.
After that the region is cycled again and the appropriate spacing is inserted."
  (interactive "r\ncCharacter to align to: ")
  (let (bef aft
            (char (char-to-string character)))
    (goto-char beg)
    (save-excursion
      (while (> end (point))
        (when (search-forward char (line-end-position) t)
          (backward-char)
          (when (or (not bef)
                    (< bef (current-column)))
            (setq bef (current-column))
            (forward-char)
            (while (string= " " (string (following-char)))
              (forward-char))
            (when (or (not aft)
                      (< aft (current-column)))
              (setq aft (current-column)))))
        (goto-char (line-end-position))
        (when (> end (point))
          (next-line))))
    (while (> end (point))
      (when (search-forward char (line-end-position) t)
        (backward-char)
        (while (< (current-column) bef)
          (insert " ")))
      (goto-char (line-end-position))
      (when (> end (point))
        (next-line)))))

(defun my-open-last-migration ()
  "Open last migration file in db/migrate/"
  (interactive)
  (let* (path-list
         root-found-p
         sliced
         build
         (root-dirs '("app" "bin" "config" "db" "lib" "log" "public" "spec" "tmp" "vendor"))
         (bfile (or (buffer-file-name) default-directory))
         (target "db/migrate/")
         (direct (file-exists-p target))
         (last (lambda (file)
                 (car (last (directory-files file))))))
    (if direct
        (find-file (concat bfile target (funcall last target)))
      (if bfile
          (progn
            (setq path-list (split-string bfile "/"))
            (dolist (el path-list)
              (let ((hold (member el root-dirs)))
                (if hold
                    (progn
                      (setq sliced hold)
                      (setq root-found-p t))
                  (unless root-found-p
                    (setq build (concat build el "/"))))))
            (if (and root-found-p sliced)
                (let* ((path (concat build target))
                       (fname (funcall last path))
                       (full (concat path fname)))
                  (when full
                    (find-file full)))
              (mmessage "Invalid path...")))
        (mmessage "Error!")))))

(defun mmessage (thing)
  "Simple wrapper around message %s [var]."
  (message "%s" thing))

(defun my-pry ()
  "Insert binding.pry in ruby or slim file."
  (interactive)
  (let ((file buffer-file-name))
    (when file
      (let ((ext (car (last (split-string file "\\."))))
            (cmd "binding.pry"))
        (if (string= "slim" ext)
            (insert "- " cmd)
          (insert cmd))))))

(defun my-set-current-project ()
  "Temporarily set the current project path to whatever the current
directory is (determined by default-directory). This is used in order
to navigate to a project directory, set it as the current project, then
simply source init again to reset the current project."
  (interactive)
  (let ((dir default-directory))
    (if dir
        (progn
          (setq my-current-project-path dir)
          (message "Directory: [ %s ] set as current project" dir))
      (message "Error! Could not set [ %s ] as default directory" dir))))

(defun my-open-c-or-header-file ()
  "Opens the corresponding c file for header or vice versa. This only works when
currently viewing either a *.c, or *.h file, or any other file that has a *.h file
with the same name in the same directory. The search does not expand from the current
directory of the currently open file that the function is called from."
  (interactive)
  (let ((f buffer-file-name))
    (if f
        (let* (target
               (path (car (last (split-string f "/"))))
               (file (split-string path "\\."))
               (name (car file))
               (ext (car (cdr file))))
          (if (string= "h" ext)
              (setq target (concat name ".c"))
            (setq target (concat name ".h")))
          (if (file-exists-p target)
              (find-file target)
            (message "File [ %s ] does not exist." target)))
      (message "Invalid."))))

(defun my-tochar-forward (count char)
  "Navigate to char forward. This takes a single char as an argument and
does not have to be on the same line as point."
  (interactive "p\ncForward to: ")
  (let ((c (char-to-string char)))
    (if (search-forward c nil t count)
        (backward-char)
      (message "%s not found!" c))))

(defun my-tochar-backward (count char)
  "Navigate to char backward. This takes a single char as an argument and
does not have to be on the same line as point."
  (interactive "p\ncBackward to: ")
  (let ((c (char-to-string char)))
    (unless (search-backward c nil t count)
      (message "%s not found!" c))))

(defun my-toggle-solarized ()
  "Toggle solarized dark/light theme based on what's currently loaded. We are
assuming the default is solarized-dark so that works as the jumping off point."
  (interactive)
  (let ((theme (custom-theme-enabled-p 'solarized-dark)))
    (if theme
        (if (or (eq (length theme) 2)
                (and (eq (length theme) 1)
                     (not (custom-theme-enabled-p 'solarized-light))))
            (load-theme 'solarized-light)
          (load-theme 'solarized-dark))
      (load-theme 'solarized-dark))
    (set-cursor-color "red")))

(defun my-view-staging ()
  "View current staging server set by myv-current-staging"
  (interactive)
  (if (bound-and-true-p myv-current-staging)
      (browse-url myv-current-staging)
    (message "You do not have a staging server set as myv-current-staging.")))

(defun my-loop-buffers (func)
  "Loop through all open buffers and call a function via funcall on each buffer.
This typically takes a lambda and that lambda needs to expect the argument to be
a buffer. Therefore if file name for each buffer is expected, the call to
buffer-file-list needs to be in the lambda itself, not here."
  (interactive "aFunction to apply to open buffers: ")
  (let ((buffers (buffer-list)))
    (dolist (buffer buffers)
      (funcall func buffer))))

(defun my-cleanup-files ()
  "Check for binding.pry and console.log in all open buffers. This uses
my-loop-buffers to loop through all open buffers. Therefore, naturally,
a lambda is created to pass to the loop and the rest of the function
simply handles the presentation of that data without much else."
  (interactive)
  (let ((flag nil)
        (report (get-buffer-create "*CLEANUP*"))
        (func (lambda (buffer)
                "Append to report CLEANUP report buffer if search is successful."
                (let ((file (or (buffer-file-name buffer)
                                buffer)))
                  (set-buffer buffer)
                  (goto-char (point-min))
                  (while (or (search-forward "binding.pry" nil t)
                             (search-forward "console.log" nil t))
                    (when (not
                           (string=
                            "defuns.el"
                            (car (last (split-string file "/")))))
                      (setq flag t)
                      (let ((lno (line-number-at-pos)))
                        (save-excursion
                          (set-buffer report)
                          (insert (format "%s: %d\n" file lno))))))))))
    (save-excursion
      (set-buffer report)
      (erase-buffer))
    (my-loop-buffers func)
    (if flag
        (progn
          (if (get-buffer-window report)
              (switch-to-buffer-other-window report)
            (progn
              (my-split-below)
              (switch-to-buffer report)))
          (goto-char (point-min)))
      (message "No cleanup is needed!"))
    (symbol-value flag)))

(defun my-delete-binding ()
  "delete binding.pry above or below point where found"
  (interactive)
  (save-excursion
    (if (or (or (search-backward "binding.pry" nil t)
                (search-backward "console.log" nil t))
            (or (search-forward "binding.pry" nil t)
                (search-forward "console.log" nil t)))
        (my-kill-line)
      (message "No bindings found."))))

(defun my-wrap-by-closing-char (target final)
  "Wraps char according to my-closing-char. This means we can wrap characters
and skip the nested characters of the same value."
  (interactive "cWrap:\ncWith: ")
  (let* ((char (char-to-string target))
         (wrap (char-to-string final))
         (pair (my-closing-char target))
         (start (car pair))
         (end (car (cdr pair)))
         (set (my-set-list wrap))
         (open (car set))
         (close (car (cdr set)))
         (sset (my-set-list char))
         (sopen (car sset))
         (sclose (car (cdr sset))))
    (save-excursion
      (goto-char (- start 1))
      (insert open)
      (goto-char (+ 2 end))
      (insert close))))

(defun my-browse-localhost (port)
  "Browse localhost:PORT in browser"
  (interactive "sEnter port: ")
  (let ((route (concat "http://localhost:" port)))
    (browse-url route)))

(defun my-generate-tags ()
  "Generate tags for current structure with ctags -e"
  (interactive)
  (compile "ctags -eR * ."))

(defun my-view-path ()
  "View $PATH variable. This will parse each path entry and display
one entry on a new line in the *path* buffer."
  (interactive)
  (let* ((buffer (get-buffer-create "*path*"))
         (raw-path (shell-command-to-string "echo $PATH"))
         (sep-path (split-string raw-path ":")))
    (save-excursion
      (set-buffer buffer)
      (erase-buffer)
      (dolist (el sep-path)
        (insert el "\n")))
    (my-split-below)
    (switch-to-buffer buffer)))

(defun my-eshell-command (text)
  "Insert 'text' into eshell buffer from current project root (or open eshell)"
  (interactive "sEnter eshell command: ")
  (unless (bufferp (get-buffer "*eshell*"))
    (my-open-current-project))
  (eshell)
  (insert text))

(defun my-eshell-bundle ()
  "Insert 'bundle' into eshell buffer SEE `my-eshell-command`"
  (interactive)
  (my-eshell-command "bundle"))
