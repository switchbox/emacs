# Emacs Configuration

My personal Emacs fumblings.

## Installation
Remove .emacs.d or .emacs file from ~/ and clone repo.

## Customization
Emacs will load init.el first. If certain files aren't desired remove them from init-files-list.

### Note
When you launch Emacs the first time it will load a bunch of packages. See *auto-install.el* and remove what you don't want.
