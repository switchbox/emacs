;; package auto install if not present.
;; set package-list with any desired
;; packages, set the package-archives,
;; check to see if they are already installed,
;; and if not install each package in packge-list.
(setq  package-list '(org
                      web-mode
                      emmet-mode
                      magit
                      htmlize
                      rvm
                      rinari
                      coffee-mode
                      dictionary
                      slim-mode
                      solarized-theme
                      flycheck
                      auto-complete
                      projectile
                      sudo-edit))

(setq package-archives '(("elpa" . "http://tromey.com/elpa/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))
