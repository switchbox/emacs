;; Emacs will run this file on startup. Tried to make
;; the process of adding more files to initialization
;; as painless as possible by just creating a list
;; with the name of the file itself I want to load.
;; To not use certain aspects of .emacs.d just remove
;; or comment out from init-file-list and it won't
;; be loaded at all.
;; If custom-file isn't wanted, we still have to
;; set the variable otherwise Emacs will create
;; another file for custom vars. Keep the setq
;; declaration but comment out from init-file-list.
;;;; Will only load if file exists.

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(package-initialize)

(setq custom-file "~/.emacs.d/theme.el")
(defvar init-files-list '("auto-install"
                          "auto-mode"
                          "defuns"
                          "global-keys"
                          "custom-file"
                          "vars"
                          "work"
                          "hooks")
  "List of file names without extensions that should
be checked upon initialization. If a file with that
name exists it will load, otherwise the name is simply
ignored. This allows additional files to be used in
different environments without having to worry about
porting over those files. Basically a white list of
which files we want emacs to check for and load.")

(dolist (f init-files-list)
  (if (string= f "custom-file")
      (load custom-file)
    (let ((file (concat "~/.emacs.d/" f ".el")))
      (when (file-exists-p file)
        (load file)))))
