;; auto mode alist
;; define which major modes should be used when opening specific file types.

;; @todo this should get changed to a dolist system like the other files.

;; web
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.liquid\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.module\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.inc\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.install\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.json\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.hbs\\'" . web-mode))

;; config
(add-to-list 'auto-mode-alist '("\\.htaccess\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.rdoc\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.ini\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.cconf\\'" . conf-mode))

;; javascript
(add-to-list 'auto-mode-alist '("\\.ts\\'" . javascript-mode))

;; ruby
(add-to-list 'auto-mode-alist '("\\.yml\\'" . ruby-mode))

;; slim
(add-to-list 'auto-mode-alist '("\\.styl\\'" . slim-mode))
(add-to-list 'auto-mode-alist '("\\.haml\\'" . slim-mode))
